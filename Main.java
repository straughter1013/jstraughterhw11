import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String [] args){

        ArrayList<Integer> arr1 = new ArrayList<Integer>();

        arr1.add(-2);
        arr1.add(-3);
        arr1.add( 6);
        arr1.add( 3);

        int n = arr1.size();
        int i;
        System.out.println("Elements of the Arraylist #1 : ");

        for(i = 0; i < n; i++) {
            System.out.println(arr1.get(i) + "");

        }
        System.out.println();
        Collections.sort(arr1);
        System.out.println("ArrayList after Sorting : ");

        for ( i = 0; i < n; i++) {
            System.out.println(arr1.get(i) + "");
        }
        System.out.println();

        int min = arr1.get(0);
      

        System.out.println("Minimum is : " + min);


    }
}
